---
title: "Informations"
bg: "#f7cd01"
color: black
fa-icon: info
---

#### Informations d’ordre générale/ sites internet et contacts

[Actualités](https://www.fr.ch/cha/actualites/actualites-a-propos-de-la-situation-en-ukraine)

[Espace Femme](https://www.espacefemmes.org/)

[SEM]( https://www.sem.admin.ch/sem/fr/home.html)

[Transports publics en Suisse](https://news.sbb.ch/fr/article/110639/informations-pour-les-voyageurs-en-provenance-de-l-ukraine)

[Autres informations](https://www.fr.ch/vie-quotidienne/en-cas-de-difficultes/ukraine-hebergement-familles-daccueil-demarches-asile-et-benevolat)

[Animaux de compagnie](https://www.fr.ch/diaf/saav/actualites/procedures-pour-les-refugies-ukrainiens-accompagnes-de-chiens-ou-de-chats)

[Osons l’accueil](https://osonslaccueil.ch/)
